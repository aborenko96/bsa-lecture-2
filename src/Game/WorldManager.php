<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Entities\{
    Harbour, Ships\PlayerShip
};
use BinaryStudioAcademy\Game\Events\{
    OnDamage, OnSink, OnSunkShipAttack
};
use BinaryStudioAcademy\Game\Factories\{
    HarbourFactory, ShipFactory
};
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Interfaces\Event;
use Exception;

class WorldManager
{
    private static $harbours = [];
    private static $currentHarbour;
    private static $player;
    private static $random;

    private static function initHarbours()
    {
        $data = require __DIR__ . '/Config/harbours.php';

        foreach ($data as $key => $harbourData) {
            static::$harbours[$key] = HarbourFactory::create(array_merge($harbourData, ['id' => $key]));
        }
    }

    private static function initPlayer()
    {
        $data = require __DIR__ . '/Config/player.php';
        static::$player = ShipFactory::create(array_merge($data, ['type' => 1]));
    }

    private static function moveTo($key)
    {
        static::$currentHarbour = static::getHarbour($key);
    }

    public static function init(Random $random)
    {
        static::initHarbours();
        static::initPlayer();
        static::$currentHarbour = static::getHarbour(1);
        static::$random = $random;
    }

    public static function getRandom(): Random
    {
        return static::$random;
    }

    public static function calculateStat(int $min, int $max): int
    {
        return round($min + ($max - $min) * static::getRandom()->get());
    }

    public static function getHarbour($key)
    {
        if (array_key_exists($key, static::$harbours)) {
            return static::$harbours[$key];
        }

        throw new Exception("Harbor not found in this direction");
    }

    public static function moveToDirection($direction)
    {
        $method = "get" . ucfirst($direction);

        if (!method_exists(static::getCurrentHarbour(), $method)) {
            throw new Exception("Direction '{$direction}' incorrect, choose from: east, west, north, south");
        }

        $harbour = static::getCurrentHarbour()->$method();

        static::moveTo($harbour);
    }

    public static function moveToPirateHarbour()
    {
        static::moveTo(1);
    }

    public static function getCurrentHarbour(): Harbour
    {
        return static::$currentHarbour;
    }

    public static function whereami()
    {
        $current = static::getCurrentHarbour();

        return "Harbor {$current->getId()}: {$current->getName()}.";
    }

    public static function getPlayer(): PlayerShip
    {
        return static::$player;
    }

    public static function fight(): Event
    {
        $player = static::getPlayer();
        $enemy = static::getCurrentHarbour()->getShip();

        if ($enemy->getIsSunk()) {
            return new OnSunkShipAttack();
        }

        $playerDamage = $player->attack($enemy);
        $enemy->damage($playerDamage);

        if ($enemy->getHealth() < 1) {
            $enemy->sink();
            return new OnSink($enemy);
        }

        $enemyDamage = $enemy->attack($player);
        $player->damage($enemyDamage);

        if ($player->getHealth() < 1) {
            $player->sink();
            return new OnSink($player);
        }

        return new OnDamage($player, $enemy, $playerDamage, $enemyDamage);
    }
}
<?php

use BinaryStudioAcademy\Game\Items\{
    Money, NoItem, Rum
};

return [
    2 => [
        'name' => 'Royal Patrool Schooner',
        'strength' => [
            'min' => 2,
            'max' => 4
        ],
        'armour' => [
            'min' => 2,
            'max' => 4
        ],
        'luck' => [
            'min' => 1,
            'max' => 4
        ],
        'health' => 50,
        'hold' => [new Money, new NoItem, new NoItem],
        'type' => 2
    ],
    3 => [
        'name' => 'Royal Patrool Schooner',
        'strength' => [
            'min' => 2,
            'max' => 4
        ],
        'armour' => [
            'min' => 2,
            'max' => 4
        ],
        'luck' => [
            'min' => 1,
            'max' => 4
        ],
        'health' => 50,
        'hold' => [new Money, new NoItem, new NoItem],
        'type' => 2
    ],
    4 => [
        'name' => 'Royal Patrool Schooner',
        'strength' => [
            'min' => 2,
            'max' => 4
        ],
        'armour' => [
            'min' => 2,
            'max' => 4
        ],
        'luck' => [
            'min' => 1,
            'max' => 4
        ],
        'health' => 50,
        'hold' => [new Money, new NoItem, new NoItem],
        'type' => 2
    ],
    5 => [
        'name' => 'Royal Patrool Schooner',
        'strength' => [
            'min' => 2,
            'max' => 4
        ],
        'armour' => [
            'min' => 2,
            'max' => 4
        ],
        'luck' => [
            'min' => 1,
            'max' => 4
        ],
        'health' => 50,
        'hold' => [new Money, new NoItem, new NoItem],
        'type' => 2
    ],
    6 => [
        'name' => 'Royal Battle Ship',
        'strength' => [
            'min' => 4,
            'max' => 8
        ],
        'armour' => [
            'min' => 4,
            'max' => 8
        ],
        'luck' => [
            'min' => 4,
            'max' => 7
        ],
        'health' => 80,
        'hold' => [new Rum, new NoItem, new NoItem],
        'type' => 2
    ],
    7 => [
        'name' => 'Royal Battle Ship',
        'strength' => [
            'min' => 4,
            'max' => 8
        ],
        'armour' => [
            'min' => 4,
            'max' => 8
        ],
        'luck' => [
            'min' => 4,
            'max' => 7
        ],
        'health' => 80,
        'hold' => [new Rum, new NoItem, new NoItem],
        'type' => 2
    ],
    8 => [
        'name' => 'HMS Royal Sovereign',
        'strength' => [
            'min' => 10,
            'max' => 10
        ],
        'armour' => [
            'min' => 10,
            'max' => 10
        ],
        'luck' => [
            'min' => 10,
            'max' => 10
        ],
        'health' => 100,
        'hold' => [new Money, new Money, new Rum],
        'type' => 3
    ],
];
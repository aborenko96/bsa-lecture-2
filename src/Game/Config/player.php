<?php

use BinaryStudioAcademy\Game\Items\NoItem;

return [
    'name' => 'Player',
    'strength' => 4,
    'luck' => 4,
    'armour' => 4,
    'health' => 60,
    'hold' => [new NoItem, new NoItem, new NoItem]
];
<?php

return [
    1 => [
        'name' => 'Pirates Harbor',
        'west' => 3,
        'east' => 0,
        'south' => 2,
        'north' => 4,
        'type' => 1
    ],
    2 => [
        'name' => 'Southhampton',
        'west' => 3,
        'east' => 7,
        'south' => 0,
        'north' => 1,
        'type' => 2
    ],
    3 => [
        'name' => 'Fishguard',
        'west' => 0,
        'east' => 1,
        'south' => 2,
        'north' => 4,
        'type' => 2
    ],
    4 => [
        'name' => 'Salt End',
        'west' => 3,
        'east' => 5,
        'south' => 1,
        'north' => 0,
        'type' => 2
    ],
    5 => [
        'name' => 'Isle of Grain',
        'west' => 4,
        'east' => 6,
        'south' => 7,
        'north' => 0,
        'type' => 2
    ],
    6 => [
        'name' => 'Grays',
        'west' => 5,
        'east' => 0,
        'south' => 8,
        'north' => 0,
        'type' => 2
    ],
    7 => [
        'name' => 'Felixstowe',
        'west' => 2,
        'east' => 8,
        'south' => 0,
        'north' => 5,
        'type' => 2
    ],
    8 => [
        'name' => 'London Docks',
        'west' => 7,
        'east' => 0,
        'south' => 0,
        'north' => 6,
        'type' => 2
    ]
];

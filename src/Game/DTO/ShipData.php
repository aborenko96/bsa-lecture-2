<?php

namespace BinaryStudioAcademy\Game\DTO;

class ShipData
{
    private $name;
    private $strength;
    private $armour;
    private $luck;
    private $health;
    private $hold;

    public function __construct(string $name, int $strength, int $armour, int $luck, int $health, array $hold)
    {
        $this->name = $name;
        $this->strength = $strength;
        $this->armour = $armour;
        $this->luck = $luck;
        $this->health = $health;
        $this->hold = $hold;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getHold(): array
    {
        return $this->hold;
    }
}
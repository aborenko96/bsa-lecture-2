<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\CommandFactory;
use BinaryStudioAcademy\Game\Helpers\Random;
use Throwable;

class CommandProcessor
{
    private static $isFinished = false;

    private static function process(Writer $writer, Reader $reader)
    {
        $input = trim($reader->read());

        list($command, $param) = array_pad(preg_split('/\s+/', $input), 2, null);

        try {
            $commandObject = CommandFactory::create(['name' => $command]);
            $commandObject->execute($writer, $param);
        } catch (Throwable $e) {
            echo $e->getMessage();
            $writer->writeln("Command '$command' not found");
        }
    }

    public static function executeLoop(Writer $writer, Reader $reader)
    {
        while (!static::$isFinished) {
            static::process($writer, $reader);
        }
    }

    public static function execute(Writer $writer, Reader $reader)
    {
        static::process($writer, $reader);
    }

    public static function finishGame()
    {
        static::$isFinished = true;
    }
}
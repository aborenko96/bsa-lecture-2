<?php

namespace BinaryStudioAcademy\Game\Interfaces;

interface Factory
{
    static function create(array $data = []);
}
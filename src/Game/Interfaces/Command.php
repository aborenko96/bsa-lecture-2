<?php

namespace BinaryStudioAcademy\Game\Interfaces;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface Command
{
    function execute(Writer $writer, ?String $param);
}
<?php

namespace BinaryStudioAcademy\Game\Interfaces;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface Event
{
    function handle(Writer $writer);
}
<?php

namespace BinaryStudioAcademy\Game\Interfaces;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface Strategy
{
    function execute(Writer $writer, array $params = []);
}
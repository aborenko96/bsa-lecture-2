<?php

namespace BinaryStudioAcademy\Game\Entities;

use BinaryStudioAcademy\Game\Abstracts\Ship;

class Harbour
{
    private $id;
    private $ship;
    private $type;
    private $name;
    private $west;
    private $east;
    private $north;
    private $south;

    public function __construct($id, $name, $type, $east, $west, $north, $south)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->east = $east;
        $this->west = $west;
        $this->north = $north;
        $this->south = $south;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEast(): int
    {
        return $this->east;
    }

    public function getWest(): int
    {
        return $this->west;
    }

    public function getNorth(): int
    {
        return $this->north;
    }

    public function getSouth(): int
    {
        return $this->south;
    }

    public function setShip(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }

    public function getType()
    {
        return $this->type;
    }
}
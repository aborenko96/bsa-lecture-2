<?php

namespace BinaryStudioAcademy\Game\Entities\Ships;

use BinaryStudioAcademy\Game\Abstracts\{
    Item, Ship
};
use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademy\Game\Items\{
    NoItem, Money, Rum
};

class PlayerShip extends Ship
{
    const DEFAULT_HEALTH = 60;

    protected $type = 1;

    public function addToHold(Item $item)
    {
        $this->hold[array_search(new NoItem, $this->hold)] = $item;
    }

    public function clearHold(): PlayerShip
    {
        $this->hold = [new NoItem(), new NoItem(), new NoItem()];

        return $this;
    }

    public function getFreeHoldSpaceSize(): int
    {
        return count(array_filter($this->getHold(), function ($item) {
            return $item instanceof NoItem;
        }));
    }

    public function heal($points)
    {
        $this->health = min(Stats::MAX_HEALTH, $this->health + $points);
    }

    public function restoreHealth()
    {
        $this->health = static::DEFAULT_HEALTH;
    }

    public function increaseStrength()
    {
        $this->strength = min(Stats::MAX_STRENGTH, ++$this->strength);
    }

    public function increaseArmour()
    {
        $this->armour = min(Stats::MAX_STRENGTH, ++$this->armour);
    }

    public function increaseLuck()
    {
        $this->luck = min(Stats::MAX_STRENGTH, ++$this->luck);
    }

    public function decreaseAllStats(): PlayerShip
    {
        $this->armour = --$this->armour;
        $this->strength = --$this->strength;
        $this->luck = --$this->luck;

        return $this;
    }

    public function getMoneyAmount(): int
    {
        return count(array_filter($this->getHold(), function ($item) {
            return $item instanceof Money;
        }));
    }

    public function getRumAmount(): int
    {
        return count(array_filter($this->getHold(), function ($item) {
            return $item instanceof Rum;
        }));
    }
}
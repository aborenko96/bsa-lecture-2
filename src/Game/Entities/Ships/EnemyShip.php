<?php

namespace BinaryStudioAcademy\Game\Entities\Ships;

use BinaryStudioAcademy\Game\Abstracts\Ship;
use BinaryStudioAcademy\Game\Items\NoItem;
use function foo\func;

class EnemyShip extends Ship
{
    protected $isBoarded = false;

    public function getIsBoarded(): bool
    {
        return $this->isBoarded;
    }

    public function board(): bool
    {
        $this->isBoarded = true;
        return true;
    }

    public function isHoldEmpty(): bool
    {
        return count($this->getHoldItems()) === 0;
    }

    public function getHoldItems()
    {
        return array_filter($this->hold,
            function ($item) {
                return !$item instanceof NoItem;
            }
        );
    }
}
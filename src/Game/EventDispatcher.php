<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Event;

class EventDispatcher
{
    public static function dispatch(Event $event, Writer $writer)
    {
        $event->handle($writer);
    }
}
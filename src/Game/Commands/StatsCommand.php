<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;

class StatsCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        $player = WorldManager::getPlayer();

        $writer->writeln("Ship stats:");
        $writer->writeln("strength: {$player->getStrength()}");
        $writer->writeln("armour: {$player->getArmour()}");
        $writer->writeln("luck: {$player->getLuck()}");
        $writer->writeln("health: {$player->getHealth()}");
        $writer->writeln("hold: [ " . implode(" ", $player->getHold()) . " ]");
    }

}
<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\Strategies\DrinkStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;

class DrinkCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        $player = WorldManager::getPlayer();
        $strategy = DrinkStrategyFactory::create(['rum' => $player->getRumAmount()]);
        $strategy->execute($writer);
    }
}
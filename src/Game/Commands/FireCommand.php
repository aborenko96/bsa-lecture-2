<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\{
    Reader, Writer
};
use BinaryStudioAcademy\Game\Factories\Strategies\AttackStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;

class FireCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        $attackStrategy = AttackStrategyFactory::create(['type' => WorldManager::getCurrentHarbour()->getType()]);
        $attackStrategy->execute($writer);
    }
}
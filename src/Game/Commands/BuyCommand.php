<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\{
    Reader, Writer
};
use BinaryStudioAcademy\Game\Factories\Strategies\BuyItemStrategyFactory;
use BinaryStudioAcademy\Game\Factories\Strategies\BuyStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;

class BuyCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        $strategy = BuyStrategyFactory::create(['type' => WorldManager::getCurrentHarbour()->getType()]);
        $strategy->execute($writer, ['param' => $param]);
    }

}
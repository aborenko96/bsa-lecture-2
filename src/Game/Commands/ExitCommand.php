<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\CommandProcessor;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Command;

class ExitCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        CommandProcessor::finishGame();
    }
}
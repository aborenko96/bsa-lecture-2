<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Command;

class HelpCommand implements Command
{
    public function execute(Writer $writer, ?String $param)
    {
        $writer->writeln('List of commands:');
        $writer->writeln('help - shows this list of commands');
        $writer->writeln('stats - shows stats of ship');
        $writer->writeln('set-sail <east|west|north|south> - moves in given direction');
        $writer->writeln('fire - attacks enemy\'s ship');
        $writer->writeln('aboard - collect loot from the ship');
        $writer->writeln('buy <strength|armour|luck|rum> - buys skill or rum: 1 chest of gold - 1 item');
        $writer->writeln('drink - your captain drinks 1 bottle of rum and fill 30 points of health');
        $writer->writeln('whereami - shows current harbor');
        $writer->writeln('exit - finishes the game');
    }
}
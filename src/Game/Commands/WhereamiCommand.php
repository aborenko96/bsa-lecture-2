<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\{
    Reader, Writer
};
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;

class WhereamiCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        $writer->writeln(WorldManager::whereami());
    }
}
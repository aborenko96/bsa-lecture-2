<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\Strategies\AboardStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;

class AboardCommand implements Command
{

    function execute(Writer $writer, ?String $param)
    {
        $aboardStrategy = AboardStrategyFactory::create(['type' => WorldManager::getCurrentHarbour()->getType()]);
        $aboardStrategy->execute($writer);
    }
}
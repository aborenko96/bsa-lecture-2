<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\Strategies\MoveStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\WorldManager;
use Throwable;

class SetSailCommand implements Command
{
    function execute(Writer $writer, ?String $param)
    {
        try {
            WorldManager::moveToDirection($param);
            $writer->writeln(WorldManager::whereami());
            $moveStratery = MoveStrategyFactory::create(['type' => WorldManager::getCurrentHarbour()->getType()]);
            $moveStratery->execute($writer);
        } catch (Throwable $e) {
            $writer->writeln($e->getMessage());
        }
    }

}
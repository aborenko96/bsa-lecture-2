<?php

namespace BinaryStudioAcademy\Game\Abstracts;

abstract class Item
{
    protected $icon;

    public function __toString()
    {
        return $this->icon;
    }
}
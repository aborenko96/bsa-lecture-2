<?php

namespace BinaryStudioAcademy\Game\Abstracts;

use BinaryStudioAcademy\Game\DTO\ShipData;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Items\NoItem;
use BinaryStudioAcademy\Game\WorldManager;

abstract class Ship
{
    protected $name;
    protected $strength;
    protected $armour;
    protected $luck;
    protected $health;
    protected $hold = [];
    protected $isSunk = false;
    protected $type;

    public function __construct(ShipData $data)
    {
        $this->name = $data->getName();
        $this->strength = $data->getStrength();
        $this->armour = $data->getArmour();
        $this->luck = $data->getLuck();
        $this->health = $data->getHealth();
        $this->hold = $data->getHold();
    }

    function getHoldItems()
    {
        return [];
    }

    function isHoldEmpty() : bool
    {
        return false;
    }

    function getIsBoarded(): bool
    {
        return false;
    }

    function board(): bool
    {
        return false;
    }

    public function sink()
    {
        $this->isSunk = true;
    }

    public function damage($damage)
    {
        $this->health = max(0, $this->health - $damage);
    }

    public function getIsSunk(): bool
    {
        return $this->isSunk;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function attack(Ship $target): int
    {
        $math = new Math();
        $random = WorldManager::getRandom();

        if ($math->luck($random, $this->getLuck())) {
            return $math->damage($this->getStrength(), $target->getArmour());
        }

        return 0;
    }

    public function removeFromHold(Item $item)
    {
        $hold = array_reverse($this->hold);
        $position = array_search($item, $hold);

        if ($position >= 0) {
            $hold[$position] = new NoItem();
            $this->hold = array_reverse($hold);
        }
    }
}
<?php

namespace BinaryStudioAcademy\Game\Abstracts;

use BinaryStudioAcademy\Game\Interfaces\Factory;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

abstract class StrategyFactory implements Factory
{
    abstract protected static function init();

    protected static $strategies = null;

    protected static function getStrategy(int $type): Strategy
    {
        return static::$strategies[$type];
    }

    public static function create(array $data = [])
    {
        if (!static::$strategies) {
            static::init();
        }

        return static::getStrategy($data['type']);
    }
}
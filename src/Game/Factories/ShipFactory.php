<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Abstracts\Ship;
use BinaryStudioAcademy\Game\DTO\ShipData;
use BinaryStudioAcademy\Game\Entities\Ships\BossShip;
use BinaryStudioAcademy\Game\Entities\Ships\PlayerShip;
use BinaryStudioAcademy\Game\Entities\Ships\RegularShip;
use BinaryStudioAcademy\Game\Interfaces\Factory;
use PHPUnit\Util\Exception;

class ShipFactory implements Factory
{
    public static function create(array $data = []): Ship
    {
        $dto = new ShipData($data['name'], $data['strength'], $data['armour'], $data['luck'], $data['health'], $data['hold']);
        $type = $data['type'];

        switch ($type) {
            case 1:
                return new PlayerShip($dto);
            case 2:
                return new RegularShip($dto);
            case 3:
                return new BossShip($dto);
            default:
                throw new Exception('Invalid ship type');
        }
    }
}
<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;

use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Strategies\Move\{
    PirateHarbourMove, RegularHarbourMove
};

class MoveStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new PirateHarbourMove(),
            2 => new RegularHarbourMove()
        ];
    }
}
<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;


use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Strategies\Aboard\{
    PirateHarbourAboard, RegularHarbourAboard
};

class AboardStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new PirateHarbourAboard(),
            2 => new RegularHarbourAboard()
        ];
    }
}
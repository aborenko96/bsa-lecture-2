<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;

use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\Strategies\BuyItem\{
    BuyRum, BuySkill, Unknown, NoMoney
};
use BinaryStudioAcademy\Game\WorldManager;

class BuyItemStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new BuyRum(),
            2 => new BuySkill(),
            3 => new Unknown(),
            4 => new NoMoney()
        ];
    }

    public static function create(array $data = [])
    {
        $param = $data['param'];

        if (!static::$strategies) {
            static::init();
        }

        if (!WorldManager::getPlayer()->getMoneyAmount()) return static::$strategies[4];
        if ($param === 'rum') return static::$strategies[1];
        if (in_array($param, ['luck', 'strength', 'armour'])) return static::$strategies[2];

        return static::$strategies[3];
    }
}
<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;

use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Strategies\Sink\{
    PlayerShipSink, RegularShipSink, BossShipSink
};

class SinkStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new PlayerShipSink(),
            2 => new RegularShipSink(),
            3 => new BossShipSink()
        ];
    }
}
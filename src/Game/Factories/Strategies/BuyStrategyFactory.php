<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;

use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Strategies\Buy\{
    PirateHarbourBuy, RegularHarbourBuy
};

class BuyStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new PirateHarbourBuy(),
            2 => new RegularHarbourBuy(),
        ];
    }
}
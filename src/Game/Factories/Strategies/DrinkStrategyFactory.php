<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;

use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Strategies\Rum\{
    NoRum, HaveRum
};

class DrinkStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new NoRum(),
            2 => new HaveRum(),
        ];
    }

    public static function create(array $data = [])
    {
        if (!static::$strategies) {
            static::init();
        }

        return static::getStrategy($data['rum'] > 0 ? 2 : 1);
    }
}
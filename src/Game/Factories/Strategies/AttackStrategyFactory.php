<?php

namespace BinaryStudioAcademy\Game\Factories\Strategies;

use BinaryStudioAcademy\Game\Abstracts\StrategyFactory;
use BinaryStudioAcademy\Game\Strategies\Attack\{
    PirateHarbourAttack, RegularHarbourAttack
};

class AttackStrategyFactory extends StrategyFactory
{
    protected static $strategies = null;

    protected static function init()
    {
        static::$strategies = [
            1 => new PirateHarbourAttack(),
            2 => new RegularHarbourAttack()
        ];
    }
}
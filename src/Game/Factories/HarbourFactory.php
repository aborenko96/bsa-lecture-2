<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Entities\Harbour;
use BinaryStudioAcademy\Game\Interfaces\Factory;

class HarbourFactory implements Factory
{
    public static function create(array $data = []) : Harbour
    {
        return new Harbour($data['id'], $data['name'], $data['type'], $data['east'], $data['west'], $data['north'], $data['south']);
    }
}
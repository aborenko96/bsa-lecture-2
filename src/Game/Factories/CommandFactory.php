<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Interfaces\Command;
use BinaryStudioAcademy\Game\Interfaces\Factory;
use Exception;

class CommandFactory implements Factory
{
    private static $commandNamespace = "BinaryStudioAcademy\\Game\\Commands\\";

    static function create(array $data = []) : Command
    {
        $class = static::$commandNamespace . implode(array_map("ucfirst", explode("-", $data['name']))) . "Command";

        if(class_exists($class)) {
            return new $class;
        }

        throw new Exception();
    }

}
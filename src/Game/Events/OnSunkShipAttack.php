<?php

namespace BinaryStudioAcademy\Game\Events;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Event;

class OnSunkShipAttack implements Event
{
    function handle(Writer $writer)
    {
        $writer->writeln("Can not attack already sunk ship");
    }
}
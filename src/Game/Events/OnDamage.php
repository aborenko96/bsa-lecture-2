<?php

namespace BinaryStudioAcademy\Game\Events;

use BinaryStudioAcademy\Game\Abstracts\Ship;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Event;

class OnDamage implements Event
{
    private $player;
    private $enemy;
    private $playerDamage;
    private $enemyDamage;

    public function __construct(Ship $player, Ship $enemy, int $playerDamage, int $enemyDamage)
    {
        $this->player = $player;
        $this->enemy = $enemy;
        $this->playerDamage = $playerDamage;
        $this->enemyDamage = $enemyDamage;
    }

    function handle(Writer $writer)
    {
        $writer->writeln("{$this->enemy->getName()} has damaged on: {$this->playerDamage} points.");
        $writer->writeln("health: {$this->enemy->getHealth()}");
        $writer->writeln("{$this->enemy->getName()} damaged your ship on: {$this->enemyDamage} points.");
        $writer->writeln( "health: {$this->player->getHealth()}");
    }
}
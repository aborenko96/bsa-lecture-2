<?php

namespace BinaryStudioAcademy\Game\Events;

use BinaryStudioAcademy\Game\Abstracts\Ship;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\Strategies\SinkStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Event;

class OnSink implements Event
{
    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    function handle(Writer $writer)
    {
        $sinkStrategy = SinkStrategyFactory::create(['type'=>$this->ship->getType()]);
        $sinkStrategy->execute($writer);
    }
}
<?php

namespace BinaryStudioAcademy\Game\Events;

use BinaryStudioAcademy\Game\Abstracts\Ship;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Entities\Ships\PlayerShip;
use BinaryStudioAcademy\Game\Interfaces\Event;

class OnBoard implements Event
{
    private $player;
    private $enemy;

    public function __construct(PlayerShip $player, Ship $enemy)
    {
        $this->player = $player;
        $this->enemy = $enemy;
    }

    function handle(Writer $writer)
    {
        $this->enemy->board();
        $hold = $this->enemy->getHoldItems();

        $holdString = implode($hold);

        $writer->writeln("You got {$holdString}.");

        while ($this->player->getFreeHoldSpaceSize() > 0 && !$this->enemy->isHoldEmpty()) {
            $item = array_shift($hold);
            $this->player->addToHold($item);
            $this->enemy->removeFromHold($item);
        }

    }

}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Attack;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class PirateHarbourAttack implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln('There is no ship to fight');
    }
}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Attack;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\EventDispatcher;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\WorldManager;

class RegularHarbourAttack implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        EventDispatcher::dispatch(WorldManager::fight(), $writer);
    }

}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Rum;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\Items\Rum;
use BinaryStudioAcademy\Game\WorldManager;

class HaveRum implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $player = WorldManager::getPlayer();
        $player->removeFromHold(new Rum);
        $player->heal(30);
        $writer->writeln("You\'ve drunk a rum and your health filled to {$player->getHealth()}");
    }
}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Rum;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class NoRum implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln("You have no rum");
    }
}
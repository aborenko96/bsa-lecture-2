<?php

namespace BinaryStudioAcademy\Game\Strategies\BuyItem;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class Unknown implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln("You can only buy strength, luck, armour or rum");
    }
}
<?php

namespace BinaryStudioAcademy\Game\Strategies\BuyItem;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\Items\{
    Money, Rum
};
use BinaryStudioAcademy\Game\WorldManager;

class BuyRum implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $player = WorldManager::getPlayer();
        $player->removeFromHold(new Money);
        $player->addToHold(new Rum);
        $writer->writeln("You\'ve bought a rum. Your hold contains {$player->getRumAmount()} bottle(s) of rum.");
    }
}
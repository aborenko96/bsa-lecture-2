<?php

namespace BinaryStudioAcademy\Game\Strategies\BuyItem;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\Items\Money;
use BinaryStudioAcademy\Game\WorldManager;

class BuySkill implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $param = $params['param'];
        $method = "increase" . ucfirst($param);
        $getter = "get" . ucfirst($param);

        $player = WorldManager::getPlayer();
        $player->removeFromHold(new Money);
        $player->$method();
        $writer->writeln("You\'ve bought a {$param}. Your {$param} is {$player->$getter()}.");
    }
}
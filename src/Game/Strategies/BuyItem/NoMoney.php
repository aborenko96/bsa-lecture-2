<?php

namespace BinaryStudioAcademy\Game\Strategies\BuyItem;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class NoMoney implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln('You have no money');
    }
}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Buy;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\Strategies\BuyItemStrategyFactory;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class PirateHarbourBuy implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $strategy = BuyItemStrategyFactory::create(['param'=>$params['param']]);
        $strategy->execute($writer, ['param'=>$params['param']]);
    }
}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Buy;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class RegularHarbourBuy implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln("You can only trade at the pirates harbour");
    }
}
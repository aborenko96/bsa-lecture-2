<?php

namespace BinaryStudioAcademy\Game\Strategies\Aboard;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class PirateHarbourAboard implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln("There is no ship to aboard");
    }
}
<?php

namespace BinaryStudioAcademy\Game\Strategies\Aboard;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Entities\Ships\EnemyShip;
use BinaryStudioAcademy\Game\EventDispatcher;
use BinaryStudioAcademy\Game\Events\OnBoard;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\WorldManager;

class RegularHarbourAboard implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $ship = WorldManager::getCurrentHarbour()->getShip();
        $player = WorldManager::getPlayer();

        if ($ship->getIsSunk()) {
            if (!$ship->getIsBoarded()) {
                EventDispatcher::dispatch(new OnBoard($player, $ship), $writer);
            } else {
                $writer->writeln("Ship is already boarded");
            }
        } else {
            $writer->writeln("You cannot board this ship, since it has not yet sunk");
        }
    }
}
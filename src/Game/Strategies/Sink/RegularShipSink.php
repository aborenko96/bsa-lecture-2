<?php

namespace BinaryStudioAcademy\Game\Strategies\Sink;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\WorldManager;

class RegularShipSink implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $ship = WorldManager::getCurrentHarbour()->getShip();

        $writer->writeln("{$ship->getName()} on fire. Take it to the boarding.");
    }
}
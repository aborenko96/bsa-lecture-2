<?php

namespace BinaryStudioAcademy\Game\Strategies\Sink;

use BinaryStudioAcademy\Game\CommandProcessor;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;

class BossShipSink implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln("🎉🎉🎉Congratulations🎉🎉🎉");
        $writer->writeln("💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾");
        CommandProcessor::finishGame();
    }
}
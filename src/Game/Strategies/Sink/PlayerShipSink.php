<?php

namespace BinaryStudioAcademy\Game\Strategies\Sink;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\WorldManager;

class PlayerShipSink implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        $writer->writeln("Your ship has been sunk.");
        $writer->writeln("You restored in the Pirate Harbor.");
        $writer->writeln("You lost all your possessions and 1 of each stats.");

        WorldManager::moveToPirateHarbour();
        WorldManager::getPlayer()
            ->clearHold()
            ->decreaseAllStats()
            ->restoreHealth();
    }
}
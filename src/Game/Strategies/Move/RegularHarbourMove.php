<?php

namespace BinaryStudioAcademy\Game\Strategies\Move;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factories\ShipFactory;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\WorldManager;

class RegularHarbourMove implements Strategy
{
    private static $ships = null;

    private function init()
    {
        static::$ships = require_once __DIR__ . "/../../Config/ships.php";
    }

    function execute(Writer $writer, array $params = [])
    {
        if (!static::$ships) {
            static::init();
        }

        $shipData = static::$ships[WorldManager::getCurrentHarbour()->getId()];

        $data = [
            'type' => $shipData['type'],
            'strength' => WorldManager::calculateStat($shipData['strength']['min'], $shipData['strength']['max']),
            'armour' => WorldManager::calculateStat($shipData['armour']['min'], $shipData['armour']['max']),
            'luck' => WorldManager::calculateStat($shipData['luck']['min'], $shipData['luck']['max']),
            'health' => $shipData['health'],
            'name' => $shipData['name'],
            'hold' => $shipData['hold']
        ];

        $ship = ShipFactory::create($data);

        WorldManager::getCurrentHarbour()->setShip($ship);

        $writer->writeln("You see {$ship->getName()}: ");
        $writer->writeln("strength: {$ship->getStrength()}");
        $writer->writeln("armour: {$ship->getArmour()}");
        $writer->writeln("luck: {$ship->getLuck()}");
        $writer->writeln("health: {$ship->getHealth()}");
    }
}
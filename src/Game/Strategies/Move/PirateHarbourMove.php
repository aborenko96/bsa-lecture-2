<?php

namespace BinaryStudioAcademy\Game\Strategies\Move;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Entities\Ships\PlayerShip;
use BinaryStudioAcademy\Game\Interfaces\Strategy;
use BinaryStudioAcademy\Game\WorldManager;

class PirateHarbourMove implements Strategy
{
    function execute(Writer $writer, array $params = [])
    {
        if (WorldManager::getPlayer()->getHealth() < PlayerShip::DEFAULT_HEALTH) {
            WorldManager::getPlayer()->restoreHealth();
            $writer->writeln("Your health is repared to 60.");
        }
    }

}
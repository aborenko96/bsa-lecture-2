<?php

namespace BinaryStudioAcademy\Game\Items;

use BinaryStudioAcademy\Game\Abstracts\Item;

class Money extends Item
{
    protected $icon = "💰";
}
<?php

namespace BinaryStudioAcademy\Game\Items;

use BinaryStudioAcademy\Game\Abstracts\Item;

class NoItem extends Item
{
    protected $icon = "_";
}
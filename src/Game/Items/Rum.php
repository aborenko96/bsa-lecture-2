<?php

namespace BinaryStudioAcademy\Game\Items;

use BinaryStudioAcademy\Game\Abstracts\Item;

class Rum extends Item
{
    protected $icon = "🍾";
}